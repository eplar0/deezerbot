You can install requirements by running `pip install -r requirements.txt`

Bot needs to login to Deezer to function properly. You need to edit your config file and paste your user token in it.

#### Chrome
1. Press F12
2. Click Application
3. Click Cookies and then `https://www.deezer.com`
4. In the table, in the row `arl` copy the Value

#### Firefox
1. Press F12
2. Go to storage
3. Under Cookies click `https://www.deezer.com`
4. In the table, in the row `arl` copy the Value